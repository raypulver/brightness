#include "pnmrdr.h"

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
  FILE *file;
  switch (argc) {
    case 1:
      file = stdin;
      break;
    default:
      file = fopen(argv[1], "rb");
      break;
  }
  Pnmrdr_T reader = Pnmrdr_new(file);
  Pnmrdr_mapdata data = Pnmrdr_data(reader);
  size_t sz = data.height*data.width;
  size_t total = 0;
  int i = -1;
  while (++i < sz) {
    total += Pnmrdr_get(reader);
  }
  fclose(file);
  Pnmrdr_free(&reader);
  printf("%f\n", (double) total / sz / 0xff);
}
