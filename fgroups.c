#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "table.h"
#include "list.h"
#include "atom.h"
#define UNUSED(x) (void) (x)
void breakpt() {}
void output_group(void **x, void *cl) {
	char *word = *(char **) x;
	(void) cl;
	printf("%s\n", word);
}

void output_groups(const void *key, void **value, void *cl) {
	List_T list = *(List_T *) value;
	(void) cl;
        //printf("%s\n", (char *) key);
//        printf("%p\n", (void *) list);
	if (List_length(list) > 1) {
		List_map(list, &output_group, NULL);
		printf("\n");
	}
}

int main(int argc, char **argv) {
	int res;
	Table_T table = Table_new(64, NULL, NULL);

	while (1) {
	        char *word = malloc(65536);
		char *fingerprint = malloc(1024);
		res = scanf("%s %s\n", fingerprint, word);
		if (res == EOF) {
			free(word);
			free(fingerprint);
			break;
		}
	        const char *fingerprint_atom = Atom_new(fingerprint, strlen(fingerprint) + 1);
		void *bucket = Table_get(table, fingerprint_atom);
		if (bucket) {
                
			List_T lst_uncopied = List_push((List_T) bucket, word);
                        Table_put(table, fingerprint_atom, lst_uncopied);
		} else {
			List_T lst = List_list(word);
			Table_put(table, fingerprint_atom, lst);
		}
	}
	/*
	printf("%p\n", out);
        List_T *list = *(List_T **) out;
	void **chr;
	printf("woop\n");
	List_pop(*list, chr);
	printf("woop\n");
	char *chr_deref = *(char **) chr;
	printf("%s", chr_deref);
	*/
        Table_map(table, &output_groups, NULL);
	printf("123\n");
	UNUSED(argc);
	UNUSED(argv);
	return 0;
}
